# Import a base image
FROM node:alpine

LABEL description="Docker Image to run hugo and bulma"
LABEL maintainer="David Broderick <David.Broderick@gmail.com>"

# config
ENV HUGO_VERSION=0.62.2
#ENV HUGO_TYPE=
ENV HUGO_TYPE=_extended

ENV HUGO_ID=hugo${HUGO_TYPE}_${HUGO_VERSION}
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_ID}_Linux-64bit.tar.gz /tmp
RUN tar -xf /tmp/${HUGO_ID}_Linux-64bit.tar.gz -C /tmp \
    && mkdir -p /usr/local/sbin \
    && mv /tmp/hugo /usr/local/sbin/hugo \
    && rm -rf /tmp/${HUGO_ID}_linux_amd64 \
    && rm -rf /tmp/${HUGO_ID}_Linux-64bit.tar.gz \
    && rm -rf /tmp/LICENSE.md \
    && rm -rf /tmp/README.md

RUN apk add --update git asciidoctor libc6-compat libstdc++ \
    && apk upgrade \
    && apk add --no-cache ca-certificates 

RUN npm install --quiet -g postcss-cli
RUN npm install -g bulma

VOLUME /src
VOLUME /output

WORKDIR /src

EXPOSE 1313
